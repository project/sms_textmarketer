<?php
/**
 * @file
* Pages provided by the Textmarketer module.
*/

/**
 * Incoming messages page.
 *
 * @param string $number
 *   The number the incoming message was sent to.
 */
function sms_textmarketer_incoming($number = '') {
  // Get data provided in query parameters.
  $params = drupal_get_query_parameters();

  // Additional data passed with incoming message.
  $network = array_key_exists('network', $params);
  $options = array(
    'network' => $network ? $params['network'] : '',
  );

  // Record the number the incoming message was sent to if available.
  if (!empty($number)) {
    $options['gw_number'] = $number;
  }

  // Pass incoming message to the SMS Framework module.
  sms_incoming($params['number'], $params['message'], $options);

  return '';
}
